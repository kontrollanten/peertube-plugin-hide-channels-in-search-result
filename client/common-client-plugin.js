function register ({ registerHook, peertubeHelpers }) {
  registerHook({
    target: 'filter:api.search.video-channels.list.result',
    handler: () => ({
      data: [],
      total: 0,
    }),
  });
}

export {
  register
}
